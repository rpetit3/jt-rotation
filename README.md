Techniques for Alignment Free Sequence Comparison (afsc)
========================================================

Introduction
------------
Afsc is a paradigm for the creation of representative indexes of sequence data. Typically
these indexes are high-dimensional vectors with real coordinates.
Ideally, these indexes should retain as much of the sequence structure as possible and
be efficiently constructed and maintained. As the name of the paradigm suggests, we would
like to compare the indexes instead of the sequences.

A typical afsc method starts by defining a set of sequence features and an algorithm for extracting
them. The simplest class of features is the number of occurrences of all possible substrings of
a fixed length *k*, or *k*-mers.
These features can be efficiently extracted as follows

    Input: ordered alphabet A, sequence S from A, k, order() the order of a string
    Output: V, where V[s] = Occurrences of s in S

    for i=0 to k^(|A|) do
        V[i] = 0

    for i=0 to |S| do
      k_mer = S[i:i+k-1]
	  V[order(k_mer)] += 1
    return V

So *V* will contain the count of all *k*-mers in *S*. Since *V* is a vector
(in *R^(k^|A|)*) it is easy to compare two vectors *V*, *V'* with a distance
function (e.g., Euclidean).

Almost all other methods are variations of the above. The conceptual difference is
in the choise of the features. Some believe that there are better features than *k*-mers.
These other methods come with differences in the justification of proposed features and
often with beautiful algorithms for their computation.

The beauty and the problem of these methods is the choice of features: it is arbitrary. Thus,
typically built from intuition or abstract probabilistic models. We want to propose a technique
that automatically extracts the set of features that best represent the input sequence
when mapped in a smaller space of size linear to the seuqence itself (see work of M. Comin,
 or ask Axa what is the best we can hope. clues from the LZ algorithm?)

Idea
----
We will use an un-supervised machine learning algorithm to extract the features that optimize
some function.  Explain

  * the algorithm

  * why are these features believed to be useful (basically justify using what we have from RL)

  * compare extracted features with features from existing approaches

  * show trees built from these features

  * (BONUS!!) set up a repository in the spirit of align-, assembl- athon (e.g., -alignathon)


