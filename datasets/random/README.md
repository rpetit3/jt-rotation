## Random datasets
Generate random dataset according to some 1-order Markov model.

Models are specified in files that list all conditional
probabilities for all alphabet symbols. For example:

  
    ## first order MC. format for
    ## ab<tab>value
    ## is P(b|a) = v

    # P(a)  = 0.25
    aa  0.1
    ac  0.1
    at  0.4
    ag  0.4

    # P(a)  = 0.25
    ca  0.2
    cc  0.2
    ct  0.4
    cg  0.2

    # P(a)  = 0.25
    ga  0.2
    gc  0.1
    gt  0.1
    gg  0.6

    # P(a)  = 0.25
    ta  0.2
    tc  0.4
    tt  0.1
    tg  0.3

the lines starting with a hashtag are comments. The file can be parsed
by the random\_sequences.py script.

This directory is to contain multiple datasets (a dataset is a set of sequences). For a
dataset named *X* we keep:

  * the models of the sequences in files named *X\_<suffix>.model*, where *<suffix>* is a string of your choice

  * the sequences in files named *X\_<suffix>.fa*, in FASTA format


