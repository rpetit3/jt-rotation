#!/usr/bin/env python
"""Combine signal in a text file with one example / row
into a pandas panel

/<hdf_path>X   wide    (shape->[<# examples>,<width>])
/<hdf_path>Y   series  (shape->[<# of examples>])

"""


import sys
import logging
import argparse
import fileinput

import numpy as np
import pandas as pd

from dimer.argutils import ArgTypes
from dimer import archive, data, ops

logging.basicConfig(level=logging.DEBUG)
lg = logging.getLogger()

if __name__ != '__main__':
    lg.error("this is a script do not import")
    sys.exit(1)

parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    epilog="Taylor Lab (odenas@emory.edu)")
parser.add_argument("input", type=str,
                    help="input data, one labeled row/line")
parser.add_argument("output", type=ArgTypes.is_dspath,
                    help="Output file. " + archive.DSPEC_MSG)
parser.add_argument("--fit", action='store_true', default=False,
                    help="For each track. Fit signal in [0, 1]")
parser.add_argument("--smooth", action='store_true', default=False,
                    help=("Smooth with an inverse hyperbolic sine: "
                          "ln(x + sqrt(x^2 + 1))"))
opt = parser.parse_args()

names, vecs = [], []
with open(opt.input) as ifd:
    for line in ifd:
        cols = line.split()
        names.append(cols[0])
        vecs.append(map(float, cols[1:]))

xsig = np.array(vecs)
if opt.smooth:
    xsig = ops.inverse_hsine(xsig)
    lg.info("smoothing (min=%.4f, max=%.4f)...", xsig.min(), xsig.max())

if opt.fit:
    xsig = (1.0 + ops.fit(xsig)) / 2.0
    lg.info("fitting (min=%.4f, max=%.4f)...", xsig.min(), xsig.max())
lg.info("%s (min=%.4f, max=%.4f)...", str(xsig.shape), xsig.min(), xsig.max())

Xitems = names
Xmajor_axis = ["seq1"]
Xminor_axis = range(xsig.shape[1])
X = pd.Panel(xsig.reshape(len(names), 1, -1),
             items=Xitems, major_axis=Xmajor_axis,
             minor_axis=Xminor_axis)
logging.getLogger("dimer.data").setLevel(logging.DEBUG)
data.AnchorDataset(X, None, None).dump(opt.output)
