#!/usr/bin/env python

"compute the representation of input according to given model"

import sys
import logging
import os

import numpy as np
from theano.tensor.shared_randomstreams import RandomStreams
from dimer import archive
from dimer.data import AnnotatedDataset
from dimer.nnet import autoencoder
from dimer.argutils import ArgTypes, parser_inst

logging.basicConfig(level=logging.INFO)
lg = logging.getLogger()
logging.getLogger("dimer.nnet").setLevel(logging.INFO)
#logging.getLogger("dimer.nnet.autoencoder").setLevel(logging.DEBUG)
#logging.getLogger("dimer.nnet.config_spec").setLevel(logging.DEBUG)
#logging.getLogger("dimer.archive").setLevel(logging.INFO)


if __name__ != '__main__':
    lg.error("this is a script do not import")
    sys.exit(1)


parser = parser_inst(__doc__)
parser.add_argument("input", type=ArgTypes.is_dsarch,
                    help="Input archive. " + archive.DSPEC_MSG)
parser.add_argument("trid", type=str, help="Train id")
parser.add_argument("--layer", type=ArgTypes.is_uint, default=4,
                    help="metalabels are repr. of this layer")
parser.add_argument("--batch_size", type=ArgTypes.is_uint, default=700,
                    help="batch size")
parser.add_argument("--dry_run", action='store_true', default=False,
                    help="Dry run")
opt = parser.parse_args()


rng = np.random.RandomState()
thrng = RandomStreams(rng.randint(100000))

training_dataset = AnnotatedDataset._from_archive(opt.input, True)
input_data_type = training_dataset.X.dtype
arch, dsp = archive.split(opt.input)
model = autoencoder.AEStack._from_archive(arch, os.path.join(dsp, opt.trid),
                                          rng, thrng, input_data_type)
training_dataset.flatten()
for i in range(training_dataset.X.shape[0]):
    x = training_dataset.X[i:i+1]
    rx = model.compute_state(opt.layer, x).flatten()
    print "%s\t%s" % (training_dataset.pX.items[i],
                      " ".join(map(lambda v: "%.4f" % v, rx.tolist())))
