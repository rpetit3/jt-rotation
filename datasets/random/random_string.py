#!/usr/bin/env python

"""generate random strings. one per line"""


import sys, argparse, logging
import re
from collections import namedtuple
from itertools import product
from operator import itemgetter, attrgetter

import numpy as np

logging.basicConfig(level=logging.INFO)
log = logging.getLogger()


class MCConf_line(namedtuple("mcconf_line", "pair prob")):
    _P = re.compile("[a-zA-Z][a-zA-Z]\s+[01][.][0-9]")
    @property
    def symbol(self):
        return self.pair[0]

    @classmethod
    def _is_valid(cls, line):
        return bool(cls._P.match(line))

    @classmethod
    def _from_line(cls, line):
        comp = line.rstrip().split()
        if len(comp) != 2:
            raise ValueError("""wrong format for %s\nexpected "xx prob" """)

        return cls._make((comp[0], float(comp[1])))


class MCConf(namedtuple("mcconf", "alphabet cprobs")):
    @classmethod
    def _load(cls, fname):
        ## data is a list of the type
        ## [(AB, P(A|B)) ...]
        ## sorted by its first component
        with open(fname) as fd:
            data = sorted(map(MCConf_line._from_line,
                              filter(MCConf_line._is_valid, fd)),
                          key=itemgetter(0))

        alphabet = set(map(attrgetter("symbol"), data))
        cprobs = []
        for i in range(len(alphabet)):
            valid_idx = range(i*len(alphabet), (i+1)*len(alphabet))
            valid_lines = map(itemgetter(1),
                              filter(lambda (_, l): _ in valid_idx,
                                     enumerate(data)))
            cprobs.append(map(attrgetter("prob"), valid_lines))
        probs = map(sum, cprobs)
        assert sum(probs) == len(alphabet), sum(probs)

        return MCConf._make((sorted(list(alphabet)), cprobs))

    @classmethod
    def _dump(cls, fname):
        pass

    def P(self):
        "get transition matrix P[i,j] = P(j | i)"

        return np.array(self.cprobs)

    def pi(self):
        "stationary distribution"
        import scipy.linalg
        A = self.P()
        v = scipy.linalg.eig(A,left=True,right=False)[1][:,0]
        return v / sum(v)

    def KL(self, other):
        p = self.pi()
        q = other.pi()

        assert p.shape == q.shape
        d = 0
        for i in range(p.shape[0]):
            if p[i] == 0:
                term = 0.0
            elif q[i] == 0:
                term = np.inf
            else:
                term = log(p[i]) * log(p[i] / q[i])
            d += term
        return d

class SeqGen( object ):
    def __init__(self, distpath):
        self.model = MCConf._load(distpath)
        log.info("model:%s", str(self.model.alphabet))
        for a in range(len(self.model.alphabet)):
            log.info("P(* | %s): %s", self.model.alphabet[a],
                     str(self.model.cprobs[a]))
        log.info("pi: %s", self.model.pi())

    def iterstr(self, n, s0=None):
        if s0 is None:
            s0 = np.random.choice(self.model.alphabet, 1)[0]

        for i in range(n):
            pdist = self.model.cprobs[self.model.alphabet.index(s0)]
            s1 = np.random.choice(self.model.alphabet, 1, p=pdist)[0]
            log.debug("%s%s -> %s", s0,
                      zip(self.model.alphabet, pdist), s1)
            yield s1
            s0 = s1


def natural(v):
    if int(v) < 0:
        raise ValueError("not positive")
    return int(v)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__,
            formatter_class = argparse.ArgumentDefaultsHelpFormatter,
            epilog="Olgert Denas (Taylor Lab)")

    parser.add_argument("model", type=str,
            help="model prob specifications")
    parser.add_argument("n", type=natural, default=1,
            help="how many strings")
    parser.add_argument("-l", "--len", type=natural, default=100,
            help="string length")
    parser.add_argument("-s", "--seed", type=int, default=None,
            help="random number generation seed")
    parser.add_argument("-b", "--burnout", type=natural, default=1000,
            help="Markov chain warm up")

    opt = parser.parse_args()
    np.random.seed(opt.seed)
    sgen = SeqGen(opt.model)

    for s0 in sgen.iterstr(opt.burnout):
        pass
    log.info("burned %d steps", opt.burnout)


    for i in range(opt.n):
        s = "".join(sgen.iterstr(opt.len, s0))
        print s
        s0 = s[-1]
