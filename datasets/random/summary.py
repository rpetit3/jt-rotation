#!/usr/bin/env python

"""generate random strings. one per line"""


import sys
import argparse
import logging
import fileinput
from collections import Counter

logging.basicConfig(level=logging.INFO)
lg = logging.getLogger()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__,
            formatter_class = argparse.ArgumentDefaultsHelpFormatter,
            epilog="Olgert Denas (Taylor Lab)")

    parser.add_argument("--alp", type=str, default=None,
                        help="set alphabet to this")
    opt = parser.parse_args()

    data = "".join(fileinput.input()).replace("\n", "")

    C = Counter(data)
    S = float(len(data))
    for a in sorted(C):
        print "P(%s) = %.2f" % (a, C[a] / S)

    CC = Counter(zip(data[:-1], data[1:]))
    for a, b in sorted(CC):
        print "P(%s|%s) = %.2f" % (b, a, CC[(a,b)] / float(C[a]))
