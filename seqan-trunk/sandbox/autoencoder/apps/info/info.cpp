#include <iostream>
#include <string>
#include <set>
#include <map>
#include <math.h>
#include <assert.h>
#include <seqan/sequence.h>
#include <seqan/seq_io.h>
#include <seqan/index.h>
#include <seqan/arg_parse.h>
#include <seqan/alignment_free.h>

using namespace seqan;
using namespace std;

typedef Index< String<Dna5>, IndexEsa<> > TMyIndex;

//
// kmer count functions
//
void get_words(int depth, string base, int K, 
               String<unsigned> & kmerCounts, unsigned int & kmer) {
    if(depth == K) {
        std::cout << base << "\t" << kmerCounts[kmer] << "\n";
        kmer++;
    }
    else {
        static char bases[] = { 'A', 'C', 'G', 'T' };
        for(int i = 0; i < 4; ++i)
            get_words(depth + 1, base + bases[i], K, kmerCounts, kmer);
    }
}

void kmers(unsigned kmer_size, String<Dna5> genome) {
    for (unsigned int k = 1; k <= kmer_size; k++) {
        String<unsigned> kmerCounts;
        unsigned int kmer = 0;
        countKmers(kmerCounts, genome, k);
        get_words(0, "", k, kmerCounts, kmer);
    }
}


//
// maximal words funcitons
//
int getCount( Finder<TMyIndex> finder, String<Dna5> pattern ) {
    clear(finder);
    int j = 0;
    while (find(finder, pattern)) 
        j++;
        
    return j;
}

double kl_term(int w, int aw, int wb, int awb) {

    assert((w > 0) && (aw > 0) && (wb > 0));
    assert((w >= aw) && (w >= awb));
    assert((w >= wb) && (w>= awb));

    double po;
    double pe;

    po = awb / float(w);
    pe = (aw * wb) / float(w * w);
    if (po == 0)
        return 0.0;
    return (po * log2(po / pe));

}

void maximal_words(TMyIndex myIndex, String<Dna5> genome, unsigned kmer_size) {
    std::set< String<Dna5> > max_words;
    std::map< String<Dna5>, double> scores;
    typedef Iterator< TMyIndex, MaxRepeats >::Type TMaxRepeatIterator;
    TMaxRepeatIterator myRepeatIterator(myIndex, 1);
    Finder<TMyIndex> finder(myIndex);
    
    while (!atEnd(myRepeatIterator)) {
        size_t len = repLength(myRepeatIterator);
        if (len <= kmer_size) {
            for (unsigned int i=0; i < countOccurrences(myRepeatIterator); ++i) {
                size_t begin = getOccurrences(myRepeatIterator)[i];
                if (len+begin+1 <= length(genome) && begin > 0) {
                    Infix<String<Dna5> >::Type awb = infixWithLength(genome, begin-1, len+2);
                    max_words.insert(awb);
                }
            }
            if(!scores.count(representative(myRepeatIterator))) {
                scores[representative(myRepeatIterator)] = 0.0;
            }
        }
        ++myRepeatIterator;
    }
    
    for (std::set<String<Dna5> >::iterator it=max_words.begin(); it!=max_words.end(); ++it) {
        String<Dna5> awb = *it;
        Infix<String<Dna5> >::Type w = infix(awb, 1, length(awb)-1);
        Infix<String<Dna5> >::Type aw = infix(awb, 0, length(awb)-1);
        Infix<String<Dna5> >::Type wb = infix(awb, 1, length(awb));
        scores[w] += kl_term(getCount(finder, w),getCount(finder, aw),getCount(finder, wb),getCount(finder, awb));
    }
    
    for (std::map<String<Dna5>, double>::iterator it=scores.begin(); it!=scores.end(); ++it)
        std::cout << it->first << "\t" << it->second << "\n";
    
}

//
// LCP functions
//
void lcp(TMyIndex myIndex) {
    indexRequire(myIndex, EsaLcp());
    for ( Size<TMyIndex>::Type i=0; i<length(myIndex); ++i){
        // Only find count LCP > 0
        if (lcpAt(i,myIndex) > 0) {
            SAValue<TMyIndex>::Type p = saAt(i,myIndex);
            Finder<Index<String<Dna5>, IndexEsa<> > > esaFinder(myIndex);
            int j = 0;
            while(find(esaFinder, suffix(indexText(myIndex),p))) {
                j++;
            }
            std::cout << suffix(indexText(myIndex),p) << "\t"<< j << "\n";
        }
    }
}


// Arguement Parser
struct ModifyStringOptions {
	String<char> in;
	String<char> method;
    unsigned kmer_size;
};

seqan::ArgumentParser::ParseResult
parseCommandLine(ModifyStringOptions & options, int argc, char const ** argv) {
	seqan::ArgumentParser parser("info");
	setShortDescription(parser, "Compute word composition of a sequence");

    addUsageLine(parser, "[\\fIOPTIONS\\fP] \\fB-i\\fP \\fIIN.FASTA\\fP");

    // Options Section: Input / Output parameters.
    addSection(parser, "Input / Output");
    addOption(parser, seqan::ArgParseOption("i", "input-file", "Name of the multi-FASTA input file.", 
                                            seqan::ArgParseArgument::INPUTFILE));
    setValidValues(parser, "input-file", "fa fasta");
    setRequired(parser, "input-file");

    addSection(parser, "General Algorithm Parameters");
	//method
    addOption(parser, seqan::ArgParseOption("m", "method", "Select method to use.", 
                                            seqan::ArgParseArgument::STRING, "METHOD"));
    setValidValues(parser, "method", "kmer lcp max");
	setRequired(parser, "method");
	//length
    addOption(parser, seqan::ArgParseOption("k", "k-mer-size", "Size of the k-mers.", 
                                            seqan::ArgParseArgument::INTEGER, "K"));
    setDefaultValue(parser, "k-mer-size", "4");

	//parse arguments
	seqan::ArgumentParser::ParseResult res = seqan::parse(parser, argc, argv);
	if (res != seqan::ArgumentParser::PARSE_OK)
		return res;

    String<char> inFileTmp, outFileTmp;
	getOptionValue(inFileTmp, parser, "input-file");
	options.in = inFileTmp;

	getOptionValue(options.kmer_size, parser, "k-mer-size");
	getOptionValue(options.method, parser, "method");
	return seqan::ArgumentParser::PARSE_OK;
}

int main(int argc, char const ** argv) {
	// parse options
	ModifyStringOptions options;
	seqan::ArgumentParser::ParseResult res = parseCommandLine(options, argc, argv);
	if (res != seqan::ArgumentParser::PARSE_OK)
		return res == seqan::ArgumentParser::PARSE_ERROR;


	// read input
    String<char> id;
    String<Dna5> genome;
    SequenceStream seqStream(toCString(options.in));
    if (!isGood(seqStream)) {
        std::cerr << "ERROR: Could not open "<< options.in<<".\n";
        return 1;
    }

    if (readRecord(id, genome, seqStream) != 0) {
        std::cerr << "ERROR: Could not read from "<< options.in<<"!\n";
        return 1;
    }

	// compute index
	if(options.method == "kmer"){        
        kmers(options.kmer_size, genome);
	} 
    else {
        TMyIndex myIndex(genome);
        
        if (options.method == "max") {
            maximal_words(myIndex, genome, options.kmer_size);
        }
        else {
            lcp(myIndex);
	    }    
    }

	return 0;
} 
